#!/usr/bin/env python3

import configparser
import json
import os

config = configparser.ConfigParser()
config['pomodoro'] = {}
pomodoro = config['pomodoro']
config['harvest'] = {}
harvest = config['harvest']

def write():
    with open('settings.ini', 'w') as configfile:
        config.write(configfile)


if not os.path.isfile('settings.ini'):
    # Set some essential initial values.

    # pomodoro['logfile'] = '~/Projects/agaric/python/pomodoroprompt/log/timelog.csv'
    pomodoro['logfile'] = ''

    # This path must be absolute, for some reason using ~ for home isn't working:
    pomodoro['logpath'] = '/home/mlncn/Projects/agaric/python/pomodoroprompt/log/'
    write()

config.read('settings.ini')


def pomodoro_logfile():
    return config['pomodoro']['logfile']

def pomodoro_logpath():
    return config['pomodoro']['logpath']

def pomodoro_latest_recorded(timestamp = None):
    if timestamp:
        config.set('pomodoro', 'second_latest_recorded', pomodoro.get('latest_recorded', None))
        config.set('pomodoro', 'latest_recorded', str(timestamp))
        write()
    else:
        return pomodoro.get('latest_recorded', None)

def harvest_set_projects_clients_map(projects_clients_map = {}):
    config.set('harvest', 'projects_clients', json.dumps(projects_clients_map))
    write()

def harvest_get_projects_clients_map():
    return json.loads(harvest['projects_clients'])
