import os
import json
import urllib.request
# Import our local settings management.
import settings

# Make backup file
import shutil
shutil.copyfile('settings.ini', 'settings.bak')

# Allow HARVEST_ACCESS_TOKEN etc to be loaded from a .env file.
from dotenv import load_dotenv
load_dotenv()

url = "https://api.harvestapp.com/v2/projects?is_active=true"
headers = {
    "User-Agent": "Python Harvest API",
    "Authorization": "Bearer " + os.environ.get("HARVEST_ACCESS_TOKEN"),
    "Harvest-Account-ID": os.environ.get("HARVEST_ACCOUNT_ID")
}

request = urllib.request.Request(url=url, headers=headers)
response = urllib.request.urlopen(request, timeout=5)
responseBody = response.read().decode("utf-8")
api_projects = json.loads(responseBody)

if (len(api_projects["projects"]) == 100):
    print("You retrieved exactly 100 projects, the API limit.  Harvest probably has more.  Adjust this script to get additional pages of results or archive some projects!")

projects = {}
for project in api_projects["projects"]:
    # Specialcase workaround not use Zeit projects directly, we bill Agaric EK.
    if not project["client"]["name"] == "ZEIT ONLINE":
      projects[project["name"]] = project["client"]["name"]

settings.harvest_set_projects_clients_map(projects)
