# Parse timelogs for upload

A script for converting basic timelogs to the formats for timetracking import.

Initially for [Pomodoro Prompt](https://gitlab.com/agaric/python/pomodoroprompt) to Harvest and [Business Tracker](https://gitlab.com/novawebdevelopment/business-tracker)

Pmodoro Prompt is extremely simplistic, and only has a description field and automatically saves the date.  The time unit for each entry is half an hour.

## Installation

### Prerequisites

#### Install venv and pip- and python, too!

```
sudo apt install python3-venv python3-pip
```

This will also install python3 if it isn't already.

We don't use venv in these instructions but you can if you want to sort of sandbox this project.

Pip is needed.

#### Make Python 3 the default

```
sudo su
update-alternatives --install /usr/bin/python python /usr/bin/python3 1
exit
```

If you don't do the above, substitute `python3` for `python` in the following.

### Install

```
mkdir -p ~/Projects/agaric/python                                                                              
git clone git@gitlab.com:agaric/python/parse-timelogs-for-upload.git
cd parse-timelogs-for-upload
python -m pip install --user -r requirements.txt
```

### Create local environments file

In a `.env` file, put your Harvest account ID and access token, both of which you can get at https://id.getharvest.com/

```
HARVEST_ACCESS_TOKEN=12345.pt.6W7wKRJEsG73NaNwBWBhv_5rQz1YkiC7_0U-OuYNnYZlMh4xP-HvmloBlrFcpJ5ZbT666HJOhNo3tXispFz4wk
HARVEST_ACCOUNT_ID=123456
```

## Usage

```bash
python pomodoro_to_harvest.py
```

## Background notes

To import into a timetracking system of any sophistication, we need to parse our description and 

Harvest allows CSV import, with a bunch of annoying fields.

If project doesn't exist it will create a new project.

# Learning from this script and continuing development

Rather than having to type out all 40 plus lines of data processing, you can also run the whole script in the interactive shell and play with it:

After typing `python` to get the interactive Python shell in this directory, you can do this line:

```python
exec(open('pomodoro_to_harvest.py').read())
```

And now you can interact with the resulting timelog DataFrame:

```python
timelog.query("time>30").loc[:100,["description","time","orig_desc"]].tail(50)
```

Or the slightly more processed tl DataFrame, for example to get the hours worked per project:

```python
tl.groupby("project").agg({"time": "sum"})["time"]/60
```

And yeah you can just sort of tack on the column you want to mess with and do an operation like that!

##### List tasks that had no properly defined project

```python
tl[tl.project == ""]
```

#### See which tasks we ended up with

```python
pd.unique(harvest.Task)
```
